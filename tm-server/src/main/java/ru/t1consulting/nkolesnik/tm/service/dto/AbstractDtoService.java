package ru.t1consulting.nkolesnik.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDTO;

@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IDtoService<M> {

    @NotNull
    protected final IConnectionService connectionService;

}
